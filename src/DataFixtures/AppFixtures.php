<?php

namespace App\DataFixtures;

use App\Entity\Type;
use App\Entity\Spends;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{

    public function __construct(UserPasswordHasherInterface $hash)
    {
        $this->hasher = $hash;
    }
    
    public function load(ObjectManager $manager)
    {
        // create 30 spends! Bam!
        for ($i = 0; $i < 30; $i++) {
            $spend = new Spends();
            $spend->setName('spends '.$i);
            $spend->setDescription('description de la spend'.$i);
            $spend->setAmount(mt_rand(250, 10000));
            $spend->setCreatedAt(new \DateTimeImmutable('now'));
            $manager->persist($spend);
        }

        for ($i = 0; $i < 5; $i++) {
            $type = new Type();
            $type->setName('type '.$i);
            $type->setDescription('description du type'.$i);
            $manager->persist($type);
        }

            //user admin
            $user = new User();
            $user->setUsername('admin');
            $user->setEmail('admin@admin.com');
            $password = $this->hasher->hashPassword(
                $user,
                "admin"
            );
            $user->setPassword($password);
            $user->setRoles(["ROLE_ADMIN"]);
            $manager->persist($user);


        for ($i = 0; $i < 25; $i++) {
            //user utilisateur
            $user = new User();
            $user->setUsername('user'.$i);
            $user->setEmail(mt_rand(10000000, 20000000).'@user.com'.$i);
            $password = $this->hasher->hashPassword($user, "user");
            $user->setPassword($password);
            $user->setRoles(["ROLE_USER"]);
            $manager->persist($user); 
        }

        $manager->flush();
    }
}
<?php

namespace App\Repository;


use App\Entity\Spends;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Spends|null find($id, $lockMode = null, $lockVersion = null)
 * @method Spends|null findOneBy(array $criteria, array $orderBy = null)
 * @method Spends[]    findAll()
 * @method Spends[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpendsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Spends::class);
    }

    public function findLastMonth($id)
    {
        $date = new \DateTime();
        $date->modify('-30 days');

        $requestGetLastMonth = $this->createQueryBuilder('e')
            ->select('e')
            ->where('e.user = :user')
            ->setParameter('user', $id)
            ->andWhere('e.createdAt > :date')
            ->setParameter(':date', $date)
            ->getQuery();

        return $requestGetLastMonth->getResult();
    }

    

    public function MonthPerMonth($id, $month, $year) 
    {

        $getMonthPerMonth = $this->createQueryBuilder('e')
            ->select('e')
            ->where('e.user = :user')
            ->setParameter(':user', $id)
            ->andWhere('MONTH(e.createdAt) = :month')
            ->setParameter(':month', $month)
            ->andWhere('YEAR(e.createdAt) = :year')
            ->setParameter(':year', $year)
            ->getQuery();

        return $getMonthPerMonth->getResult();
    }

    public function findByType($id, $type)
    {

        $getLastPerType = $this->createQueryBuilder('e')
            ->select('e')
            ->where('e.user = :user')
            ->setParameter('user', $id)
            ->andWhere('e.type = :type')
            ->setParameter(':type', $type)
            ->getQuery();

        return $getLastPerType->getResult();
    }

    // /**
    //  * @return Spends[] Returns an array of Spends objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Spends
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

namespace App\Controller;

use App\Repository\SpendsRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LastMonthSpendsController extends AbstractController
{
    #[Route('/api/user/{id}/lastmonth', name: 'last_month_expenses')]
    public function lastMonthSpends(SpendsRepository $repo, SerializerInterface $serializer, $id): Response
    {
        $spends = $repo->findLastMonth($id);
        
        $get = $serializer->serialize($spends, 'json');

        $response = new Response($get, 200, [
            "Content-type" => "application/json"
        ]);

        return $response;
    }
    #[Route('/api/user/{id}/{month}/{year}', name: 'month_per_month')]
    public function monthPerMonth(SpendsRepository $repo, SerializerInterface $serializer, $id, $month, $year): Response 
    {
        $spends = $repo->MonthPerMonth($id, $month, $year);

        $get = $serializer->serialize($spends, 'json');

        $response = new Response($get, 200, [
            "Content-type" => "application/json"
        ]);

        return $response;
    }

    #[Route('/api/user/{id}/{type}', name: 'get_per_type')]
    public function getPerType(SpendsRepository $repo, SerializerInterface $serializer, $id, $type): Response
    {
        $type = $repo->findByType($id, $type);

        $get = $serializer->serialize($type, 'json');

        $response = new Response($get, 200, [
            "Content-type" => "application/json"
    ]);
        return $response;
    }

}
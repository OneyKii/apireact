<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\TypeRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=TypeRepository::class)
 */
#[ApiResource]
class Type
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    #[Groups(["read"])]
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Groups(["read"])]
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    #[Groups(["read"])]
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=Spends::class, mappedBy="type")
     */
    private $spends;

    public function __construct()
    {
        $this->spends = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Spends[]
     */
    public function getSpends(): Collection
    {
        return $this->spends;
    }

    public function addSpend(Spends $spend): self
    {
        if (!$this->spends->contains($spend)) {
            $this->spends[] = $spend;
            $spend->setType($this);
        }

        return $this;
    }

    public function removeSpend(Spends $spend): self
    {
        if ($this->spends->removeElement($spend)) {
            // set the owning side to null (unless already changed)
            if ($spend->getType() === $this) {
                $spend->setType(null);
            }
        }

        return $this;
    }
}

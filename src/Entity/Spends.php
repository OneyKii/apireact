<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use App\Repository\SpendsRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=SpendsRepository::class)
 */
#[ApiResource(
    normalizationContext: ['groups' => ['read']],
)]

class Spends
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    #[Groups(["read"])]
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Groups(["read"])]
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    #[Groups(["read"])]
    private $description;

    /**
     * @ORM\Column(type="float")
     */
    #[Groups(["read"])]
    private $amount;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    #[Groups(["read"])]
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=Type::class, inversedBy="spends")
     */
    #[Groups(["read"])]
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="spends")
     */
    #[Groups(["read"])]
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function setType(?Type $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}

<?php

namespace ContainerX9YiOnE;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder6b11b = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer062e9 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties603de = [
        
    ];

    public function getConnection()
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'getConnection', array(), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'getMetadataFactory', array(), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'getExpressionBuilder', array(), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'beginTransaction', array(), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'getCache', array(), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->getCache();
    }

    public function transactional($func)
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'transactional', array('func' => $func), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'wrapInTransaction', array('func' => $func), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'commit', array(), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->commit();
    }

    public function rollback()
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'rollback', array(), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'getClassMetadata', array('className' => $className), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'createQuery', array('dql' => $dql), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'createNamedQuery', array('name' => $name), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'createQueryBuilder', array(), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'flush', array('entity' => $entity), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'clear', array('entityName' => $entityName), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->clear($entityName);
    }

    public function close()
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'close', array(), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->close();
    }

    public function persist($entity)
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'persist', array('entity' => $entity), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'remove', array('entity' => $entity), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'refresh', array('entity' => $entity), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'detach', array('entity' => $entity), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'merge', array('entity' => $entity), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'getRepository', array('entityName' => $entityName), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'contains', array('entity' => $entity), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'getEventManager', array(), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'getConfiguration', array(), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'isOpen', array(), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'getUnitOfWork', array(), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'getProxyFactory', array(), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'initializeObject', array('obj' => $obj), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'getFilters', array(), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'isFiltersStateClean', array(), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'hasFilters', array(), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return $this->valueHolder6b11b->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer062e9 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder6b11b) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder6b11b = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder6b11b->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, '__get', ['name' => $name], $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        if (isset(self::$publicProperties603de[$name])) {
            return $this->valueHolder6b11b->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6b11b;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder6b11b;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6b11b;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder6b11b;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, '__isset', array('name' => $name), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6b11b;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder6b11b;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, '__unset', array('name' => $name), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6b11b;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder6b11b;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, '__clone', array(), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        $this->valueHolder6b11b = clone $this->valueHolder6b11b;
    }

    public function __sleep()
    {
        $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, '__sleep', array(), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;

        return array('valueHolder6b11b');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer062e9 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer062e9;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer062e9 && ($this->initializer062e9->__invoke($valueHolder6b11b, $this, 'initializeProxy', array(), $this->initializer062e9) || 1) && $this->valueHolder6b11b = $valueHolder6b11b;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6b11b;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6b11b;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
